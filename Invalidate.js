
let data3 = require("./IsValidField.js").map((currentObject) => {
    let ArrayOfDate = currentObject.issue_date.split("/");

    if (parseInt(ArrayOfDate[0]) < 3)
        currentObject["Is_Valid"] = false;
    else
        currentObject["Is_Valid"] = true;

    return currentObject;
})


module.exports = data3;


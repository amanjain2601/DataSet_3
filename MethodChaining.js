
const data = require("./data3.js");

let answer =

    data.map((currentObject) => {
        let cloneCurrentObject={...currentObject};
        cloneCurrentObject["CVV"] = Math.floor(Math.random() * (999 - 100 + 1)) + 100;
        return cloneCurrentObject;
    })
        .map((currentObject) => {
            let cloneCurrentObject={...currentObject}
            cloneCurrentObject["Is_Valid"] = true;//assign default value.
            return cloneCurrentObject;
        })

        .map((currentObject) => {
            let ArrayOfDate = currentObject.issue_date.split("/");

            if (parseInt(ArrayOfDate[0]) < 3)
                currentObject["Is_Valid"]= false;

            return currentObject;
        })

        .sort(function (a, b) {
            return new Date(a.issue_date) - new Date(b.issue_date);
        }
        )
        .reduce((object, currentObject) => {
            let ArrayOfDate = currentObject.issue_date.split("/");

            if (object[ArrayOfDate[0]] == undefined) {
                object[ArrayOfDate[0]] = [];
                object[ArrayOfDate[0]].push(currentObject);
            }
            else
                object[ArrayOfDate[0]].push(currentObject);

            return object;
        }, {})

console.log(answer);